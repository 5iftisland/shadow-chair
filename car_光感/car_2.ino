#include <Wire.h>
int BH1750_address = 0x23,BH1750_address_1 = 0x5c; // i2c Addresse
byte buff[2];

const int motorA_1 = 3;
const int motorA_2 = 5;
const int motorB_1 = 6;
const int motorB_2 = 9;
const int motorC_1 = 10;
const int motorC_2 = 11;

const int CE_A=2;
const int CE_B=7;
const int CE_C=12;

void setup() {
  // put your setup code here, to run once:
  pinMode(motorA_1,OUTPUT);
  pinMode(motorA_2,OUTPUT);
  pinMode(motorB_1,OUTPUT);
  pinMode(motorB_2,OUTPUT);
  pinMode(motorC_1,OUTPUT);
  pinMode(motorC_2,OUTPUT);
  pinMode(CE_A,OUTPUT);
  pinMode(CE_B,OUTPUT);
  pinMode(CE_C,OUTPUT);
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);

  digitalWrite(CE_A,HIGH);
  digitalWrite(CE_B,HIGH);
  digitalWrite(CE_C,HIGH);

  Wire.begin();
  BH1750_Init(BH1750_address);
    BH1750_Init(BH1750_address_1);

  delay(200);
  Serial.begin(9600);
  Serial.println("Starte Beleuchtungsstaerkemessung - blog.simtronyx.de");
  
}

void loop() {
  // put your main code here, to run repeatedly:

  float valf=0,valf_1=0;

  if(BH1750_Read(BH1750_address)==2){
    
    valf=((buff[0]<<8)|buff[1])/1.2;
    
    if(valf<0) Serial.print("> 65535");
    else{     Serial.print("l: "); 
     Serial.print((int)valf,DEC); 
    }
    Serial.println(" lx"); 
  }
  delay(10);
  if(BH1750_Read(BH1750_address_1)==2){
    
    valf_1=((buff[0]<<8)|buff[1])/1.2;
    
    if(valf_1<0) Serial.print("> 65535");
    else{
     Serial.print("r: "); 
      Serial.print((int)valf_1,DEC); 
    }
    Serial.println(" lx"); 
  }
  delay(10);
  
  if(valf > 100){
    motor_moveA(2,190);
    motor_moveB(2,200);
    motor_moveC(1,200);
  }else{
    motor_moveA(0,0);
    motor_moveB(0,0);
    motor_moveC(0,0);
  }
}


void BH1750_Init(int address){
  Wire.beginTransmission(address);
  Wire.write(0x10); // 1 [lux] aufloesung
  Wire.endTransmission();
}

byte BH1750_Read(int address){
  
  byte i=0;
  Wire.beginTransmission(address);
  Wire.requestFrom(address, 2);
  while(Wire.available()){
    buff[i] = Wire.read(); 
    i++;
  }
  Wire.endTransmission();  
  return i;
}


void motor_moveA(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorA_1,LOW);
      digitalWrite(motorA_2,LOW);
    }
    if(motor_dir==1){
       analogWrite(motorA_1,motor_speed);
       digitalWrite(motorA_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorA_1,LOW);
      analogWrite(motorA_2,motor_speed);
    }
}
void motor_moveB(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorB_1,LOW);
      digitalWrite(motorB_2,LOW);
    }
    if(motor_dir==1){
      analogWrite(motorB_1,motor_speed);
      digitalWrite(motorB_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorB_1,LOW);
      analogWrite(motorB_2,motor_speed);
    }
}
void motor_moveC(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorC_1,LOW);
      digitalWrite(motorC_2,LOW);
    }
    if(motor_dir==1){
       analogWrite(motorC_1,motor_speed);
      digitalWrite(motorC_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorC_1,LOW);
      analogWrite(motorC_2,motor_speed);
    }
}

void Forward()
{
   motor_moveA(0,0);
   motor_moveB(1,255);
   motor_moveC(2,255);
}
void Back()
{
   motor_moveA(0,0);
   motor_moveB(2,255);
   motor_moveC(1,255);
}
void Left()
{
   motor_moveA(1,255);
   motor_moveB(0,0);
   motor_moveC(2,220);
}

void Right()
{
   motor_moveA(2,255);
   motor_moveB(1,220);
   motor_moveC(0,0);
}

