const int motorA_1 = 3;
const int motorA_2 = 5;
const int motorB_1 = 6;
const int motorB_2 = 9;
const int motorC_1 = 10;
const int motorC_2 = 11;
const int CE_A=2;
const int CE_B=7;
const int CE_C=12;
void setup()
{
  pinMode(motorA_1,OUTPUT);
  pinMode(motorA_2,OUTPUT);
  
  pinMode(motorB_1,OUTPUT);
  pinMode(motorB_2,OUTPUT);
  
  pinMode(motorC_1,OUTPUT);
  pinMode(motorC_2,OUTPUT);
  
  pinMode(CE_A,OUTPUT);
  pinMode(CE_B,OUTPUT);
  pinMode(CE_C,OUTPUT);
  
  digitalWrite(CE_A,HIGH);
  digitalWrite(CE_B,HIGH);
  digitalWrite(CE_C,HIGH);
}

void loop()
{
  Forward();

}

void motor_moveA(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorA_1,LOW);
      digitalWrite(motorA_2,LOW);
    }
    if(motor_dir==1){
       analogWrite(motorA_1,motor_speed);
      digitalWrite(motorA_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorA_1,LOW);
      analogWrite(motorA_2,motor_speed);
    }
}
void motor_moveB(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorB_1,LOW);
      digitalWrite(motorB_2,LOW);
    }
    if(motor_dir==1){
      analogWrite(motorB_1,motor_speed);
      digitalWrite(motorB_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorB_1,LOW);
      analogWrite(motorB_2,motor_speed);
    }
}
void motor_moveC(int motor_dir,int motor_speed){
    if(motor_dir==0){
      digitalWrite(motorC_1,LOW);
      digitalWrite(motorC_2,LOW);
    }
    if(motor_dir==1){
       analogWrite(motorC_1,motor_speed);
      digitalWrite(motorC_2,LOW);
    }
    if(motor_dir==2){
      digitalWrite(motorC_1,LOW);
      analogWrite(motorC_2,motor_speed);
    }
}

void Forward()
{
   motor_moveA(0,0);
   motor_moveB(2,255);
   motor_moveC(1,255);
}
void Back()
{
   motor_moveA(0,0);
   motor_moveB(1,255);
   motor_moveC(2,255);
}
void Left()//Right
{
   motor_moveA(1,235);
   motor_moveB(0,0);
   motor_moveC(2,200);
}

void Right()
{
   motor_moveA(2,230);
   motor_moveB(1,200);
   motor_moveC(0,0);
}
void RightLOWD45(){//RightULOWD45
    motor_moveA(2,255);
   motor_moveB(1,245);
   motor_moveC(0,0);

}
void LeftUP45(){
    motor_moveA(1,255);
   motor_moveB(2,245);
   motor_moveC(0,0);

}
void RightUP45(){//RightUP45
    motor_moveA(2,255);
   motor_moveB(0,0);
   motor_moveC(1,255);

}
void LeftDOWN45(){
    motor_moveA(1,255);
   motor_moveB(0,0);
   motor_moveC(2,255);

}
