#include <Wire.h>
int BH1750_address_L = 0x5c,BH1750_address_R=0X23; // i2c Addresse
byte buff_L[2],buff_R[2];

void setup() {
  pinMode(8,OUTPUT);
  digitalWrite(8,HIGH);

  Wire.begin();
    
    


  delay(200);
  Serial.begin(9600);
  Serial.println("Starte Beleuchtungsstaerkemessung - blog.simtronyx.de");
  
}

void loop() {
  // put your main code here, to run repeatedly:

  float valf=0;
  BH1750_Init(BH1750_address_L);
  if(BH1750_Read(BH1750_address_L,buff_L)==2){
    
    valf=((buff_L[0]<<8)|buff_L[1])/1.2;
    
    if(valf<0) Serial.print("> 65535");
    else Serial.print((int)valf,DEC); 
    
    Serial.println(" lx_L"); 
  }
  delay(100);
  BH1750_Init(BH1750_address_R);
  if(BH1750_Read(BH1750_address_R,buff_R)==2){
    
    valf=((buff_R[0]<<8)|buff_R[1])/1.2;
    
    if(valf<0) Serial.print("> 65535");
    else Serial.print((int)valf,DEC); 
    
    Serial.println(" lx_R"); 
  }
  delay(1000);
}


void BH1750_Init(int address){
  Wire.beginTransmission(address);
  Wire.write(0x10); // 1 [lux] aufloesung
  Wire.endTransmission();
}

byte BH1750_Read(int address,byte *buff){
  
  byte i=0;
  Wire.beginTransmission(address);
  Wire.requestFrom(address, 2);
  while(Wire.available()){
    buff[i] = Wire.read(); 
    i++;
  }
  Wire.endTransmission();  
  return i;
}



